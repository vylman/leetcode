class Solution(object):
    def maxProfitAssignment(self, difficulty, profit, worker):
        """
        :type difficulty: List[int]
        :type profit: List[int]
        :type worker: List[int]
        :rtype: int
        """
        jobs = sorted(zip(difficulty, profit))
        worker.sort()
        maxProfit = 0
        i = 0
        best = 0
        for w in worker:
            while i < len(jobs) and w >= jobs[i][0]:
                best = max(best, jobs[i][1])
                i += 1
            maxProfit += best
        return maxProfit


if __name__ == '__main__':
    s = Solution()
    # assert s.maxProfitAssignment(difficulty=[2, 4, 6, 8, 10], profit=[10, 20, 30, 40, 50], worker=[4, 5, 6, 7]) == 100
    # assert s.maxProfitAssignment(difficulty=[85, 47, 57], profit=[24, 66, 99], worker=[40, 25, 25]) == 0
    # assert s.maxProfitAssignment(difficulty=[13,37,58], profit=[4,90,96], worker=[34,73,45]) == 190
    assert s.maxProfitAssignment(difficulty=[68, 35, 52, 47, 86], profit=[67, 17, 1, 81, 3],
                                 worker=[92, 10, 85, 84, 82]) == 324
