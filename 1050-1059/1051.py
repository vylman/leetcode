class Solution(object):
    def heightChecker(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        new = sorted(heights)
        count = 0

        for i in range(len(heights)):
            if heights[i] != new[i]:
                count += 1
        return count


if __name__ == '__main__':
    s = Solution()
    assert s.heightChecker([1, 1, 4, 2, 1, 3]) == 3
    assert s.heightChecker([5, 1, 2, 3, 4]) == 5
    assert s.heightChecker([1, 2, 3, 4, 5]) == 0
