class Solution(object):
    def subarraysDivByK(self, nums, k):
        """
        Сложность O(len(nums))

        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        remainders = {0: 1}
        current_sum = 0
        count = 0

        for num in nums:
            current_sum += num
            remainder = current_sum % k
            count += remainders.get(remainder, 0)
            remainders[remainder] = remainders.get(remainder, 0) + 1

        return count

if __name__ == "__main__":
    s = Solution()
    assert s.subarraysDivByK([4, 5, 0, -2, -3, 1], 5) == 7
    assert s.subarraysDivByK([9], 5) == 0



