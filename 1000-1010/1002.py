class Solution(object):
    def commonChars(self, words):
        """
        :type words: List[str]
        :rtype: List[str]
        """
        if len(words) == 1:
            return list(words[0])
        words = sorted(words, key=len)
        set_chars = set(words[0])
        res = []

        for char in set_chars:
            n = min([word.count(char) for word in words])
            if n:
                res += [char] * n
        return res


if __name__ == '__main__':
    s = Solution()
    assert s.commonChars(["bella", "label", "roller"]) == ["e","l","l"]
    assert s.commonChars(["cool","lock","cook"]) == ["c","o"]

