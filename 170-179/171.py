class Solution(object):
    def titleToNumber(self, columnTitle):
        """
        :type columnTitle: str
        :rtype: int
        """

        res = 0
        zero_code = ord('A') - 1
        for c in columnTitle:
            res = res * 26 + ord(c) - zero_code
        return res


if __name__ == '__main__':
    s = Solution()
    assert s.titleToNumber("A") == 1
    assert s.titleToNumber("AB") == 28
    assert s.titleToNumber("ZY") == 701
