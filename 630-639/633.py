class Solution(object):
    def judgeSquareSum(self, c):
        """
        :type c: int
        :rtype: bool
        """
        left = 0
        right = int(c ** 0.5) + 1

        while left <= right:
            cur_sum = left * left + right * right

            if cur_sum == c:
                return True
            elif cur_sum < c:
                left += 1
            else:
                right -= 1

        return False


if __name__ == "__main__":
    s = Solution()
    assert s.judgeSquareSum(5) is True
    assert s.judgeSquareSum(3) is False
