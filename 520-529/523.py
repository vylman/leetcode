class Solution(object):
    def checkSubarraySum(self, nums, k):
        """
        O(n) - один раз идем по массиву и заносим остаток от деления  в словарь

        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        tmp_dct = {0: -1}

        summa = 0

        for i in range(len(nums)):
            summa += nums[i]
            rem = summa % k
            if rem in tmp_dct:
                if i - tmp_dct[rem] > 1:
                    return True
            else:
                tmp_dct[rem] = i
        return False


if __name__ == '__main__':
    s = Solution()
    assert s.checkSubarraySum([23, 2, 4, 6, 7], 6) is True
    assert s.checkSubarraySum([23, 2, 6, 4, 7], 13) is False
    assert s.checkSubarraySum([0], 1) is False
