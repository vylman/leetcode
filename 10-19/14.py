class Solution(object):
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        res = 0
        len_word_0 = len(strs[0])
        tmp_word = strs[0]

        for i in range(len_word_0):
            res += 1

            for word in strs[1:]:
                if not word.startswith(tmp_word[0:res]):
                    return strs[0][0:res-1]


        return strs[0][0:res]


if __name__ == "__main__":
    s = Solution()
    assert s.longestCommonPrefix(["flower", "flow", "flight"]) == 'fl'
    assert s.longestCommonPrefix(["dog", "racecar", "car"]) == ''
