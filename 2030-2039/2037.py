class Solution(object):
    def minMovesToSeat(self, seats, students):
        """
        :type seats: List[int]
        :type students: List[int]
        :rtype: int
        """
        moves = 0
        seats.sort()
        students.sort()

        for i in range(len(seats)):
            moves += abs(seats[i] - students[i])

        return moves


if __name__ == '__main__':
    s = Solution()
    assert s.minMovesToSeat(seats = [3,1,5], students = [2,7,4]) == 4
    assert s.minMovesToSeat(seats = [4,1,5,9], students = [1,3,2,6]) == 7
    assert s.minMovesToSeat(seats = [2,2,6,6], students = [1,3,2,6]) == 4
