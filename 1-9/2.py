# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """

        curr = head = ListNode(-1)
        additional = 0

        while l1 or l2 or additional:
            if l1:
                additional += l1.val
                l1 = l1.next

            if l2:
                additional += l2.val
                l2 = l2.next

            new_node = ListNode(additional % 10)
            curr.next = new_node
            curr = curr.next
            additional //= 10

        return head.next





