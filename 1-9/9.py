class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        if x < 0:
            return False
        return str(x) == str(x)[::-1]

    def isPalindrome2(self, x):
        """
        :type x: int
        :rtype: bool
        """
        if x < 0:
            return False
        if x < 0:
            return False
        old_x = x
        new_num = 0
        while x > 0:
            new_num = new_num * 10 + x % 10
            x = x // 10
        return new_num == old_x


if __name__ == '__main__':
    s = Solution()
    assert s.isPalindrome(10) is False
    assert s.isPalindrome2(10) is False
    assert s.isPalindrome(11) is True
    assert s.isPalindrome2(11) is True
    assert s.isPalindrome(-11) is False
    assert s.isPalindrome2(-11) is False
