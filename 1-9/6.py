class Solution(object):
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        if numRows == 1:
            return s
        rows = [[] for _ in range(numRows)]
        index = 0
        step = 1
        for i in s:
            rows[index].append(i)
            if index == numRows - 1:
                step = -1
            elif index == 0:
                step = 1
            index += step
        for i in range(numRows):
            rows[i] = ''.join(rows[i])
        return ''.join(rows)


if __name__ == '__main__':
    s = Solution()
    assert s.convert(s = "PAYPALISHIRING", numRows = 3) == "PAHNAPLSIIGYIR"
    assert s.convert(s = "PAYPALISHIRING", numRows = 4) ==  "PINALSIGYAHRPI"
