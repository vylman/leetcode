class Solution(object):
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        final = sorted(nums1 + nums2)
        length = len(final)
        if length & 1 == 1:
            return final[length // 2]
        else:
            return (final[length // 2] + final[(length // 2) - 1]) / 2.0


if __name__ == '__main__':
    s = Solution()
    assert s.findMedianSortedArrays(nums1=[1, 3], nums2=[2]) == 2
    assert s.findMedianSortedArrays(nums1=[1, 2], nums2=[3, 4]) == 2.5
