class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        max_len = 0
        current_chars = set()
        left = 0

        for right in range(len(s)):
            if s[right] not in current_chars:
                current_chars.add(s[right])
                max_len = max(max_len, right - left + 1)
            else:
                while s[right] in current_chars:
                    current_chars.remove(s[left])
                    left += 1
                current_chars.add(s[right])
        return max_len

if __name__ == '__main__':
    s = Solution()
    assert s.lengthOfLongestSubstring("abcabcbb") == 3
    assert s.lengthOfLongestSubstring("bbbbb") == 1

