class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        res = 0
        minus = 1

        if x < 0:
            minus *= -1
            x *= -1
        nums = []
        while x>0:
            tmp = x % 10
            nums.append(tmp)
            x -= tmp
            x = x/10
        mult = 1
        res += nums[-1]
        for i in nums[-2::-1]:
            res += i * (10**mult)
            mult += 1
        return res * minus

    def reverse(self, x):
        if x > 2**31-1 or x < -2**31:
            return 0
        positive = bool(x < 0)
        index = int(positive)
        str_x = str(x)[index::]
        str_x = str_x[::-1]

        res = int(str_x)
        if positive:
            res = res * -1
        if res > 2**31-1 or res < -2**31:
            return 0
        return res


if __name__ == '__main__':
    s = Solution()
    assert s.reverse(120) == 21
    assert s.reverse(-321) == -123
