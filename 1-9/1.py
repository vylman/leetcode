class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        dict_res = {}
        for i, n in enumerate(nums):
            if n in dict_res:
                return [dict_res[n], i]

            else:
                dict_res[target-n] = i


if __name__ == '__main__':
    s = Solution()
    assert s.twoSum([2, 7, 11, 15], 9) == [0, 1]

