class Solution(object):
    def replaceWords(self, dictionary, sentence):
        """
        :type dictionary: List[str]
        :type sentence: str
        :rtype: str
        """

        dictionary = sorted(dictionary, key=len)
        min_len = len(dictionary[0])

        sentence_mass = sentence.split()
        for i in range(len(sentence_mass)):
            if len(sentence_mass[i]) < min_len:
                continue
            for word in dictionary:
                if sentence_mass[i].startswith(word):
                    sentence_mass[i] = word
        return ' '.join(x for x in sentence_mass)


if __name__ == '__main__':
    s = Solution()
    assert s.replaceWords(["cat", "bat", "rat"],
                          "the cattle was rattled by the battery") == "the cat was rat by the bat"
    assert s.replaceWords(["a", "b", "c"], "aadsfasf absbs bbab cadsfafs") == "a a b c"
