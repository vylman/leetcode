class Solution(object):

    def find_group(self, hand, groupSize, i, len_hand):
        next_val = hand[i] + 1
        hand[i] = -1
        find_count = 1
        i+=1
        while i < len_hand and find_count < groupSize:
            if hand[i] == next_val:
                next_val = hand[i] + 1
                hand[i] = -1
                find_count += 1
            i += 1
        return find_count == groupSize

    def isNStraightHand(self, hand: list, groupSize: int) -> bool:
        """
        :type hand: List[int]
        :type groupSize: int
        :rtype: bool
        """
        len_hand = len(hand)
        if len_hand % groupSize:
            return False
        hand.sort()
        for i in range(len_hand):
            if hand[i] >= 0:
                if not self.find_group(hand, groupSize, i, len_hand):
                    return False
        return True


    def isNStraightHand2(self, hand, groupSize):
        """
        :type hand: List[int]
        :type groupSize: int
        :rtype: bool
        """
        if len(hand) % groupSize:
            return False
        tmp_dict = {}
        for card in hand:
            if card not in tmp_dict:
                tmp_dict[card] = 1
            else:
                tmp_dict[card] += 1

        while len(tmp_dict.keys()) > 0:
            min_card = min(tmp_dict.keys())
            for i in range(groupSize):
                if min_card not in tmp_dict.keys():
                    return False
                if tmp_dict.get(min_card) == 1:
                    tmp_dict.pop(min_card)
                else:
                    tmp_dict[min_card] -= 1
                min_card += 1
        return True


if __name__ == '__main__':
    s = Solution()
    assert s.isNStraightHand([1, 2, 3, 6, 2, 3, 4, 7, 8], 3) is True
    assert s.isNStraightHand([1, 2, 3, 4, 5], 4) is False
