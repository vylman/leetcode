class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        profit = 0
        buy = prices[0]
        sell = prices[0]

        for price in prices:
            if price < buy:
                buy = price
                sell = price
            if price > sell:
                sell = price
            if sell - buy > profit:
                profit = sell - buy
        return profit


if __name__ == '__main__':
    s = Solution()
    assert s.maxProfit(prices=[7, 1, 5, 3, 6, 4]) == 5
    assert s.maxProfit(prices=[7, 6, 4, 3, 1]) == 0
    assert s.maxProfit(prices=[1, 2]) == 1
    assert s.maxProfit(prices=[2, 1, 2, 1, 0, 1, 2]) == 2
