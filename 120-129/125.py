class Solution(object):
    def isPalindrome2(self, s):
        """
        :type s: str
        :rtype: bool
        """
        tmp = ''

        for i in s:
            if i.isalnum():
                tmp += i.lower()
        return tmp[::-1] == tmp

    def isPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        left = 0
        right = len(s)-1
        while left <= right:
            if not s[left].isalnum():
                left +=1
                continue
            if not s[right].isalnum():
                right -=1
                continue
            if s[left].lower() != (s[right]).lower():
                return False
            else:
                left+=1
                right-=1
        return True


if __name__ == "__main__":
    s = Solution()
    assert s.isPalindrome("A man, a plan, a canal: Panama") == True
    assert s.isPalindrome("race a car") == False
    assert s.isPalindrome('  ') == True
    assert s.isPalindrome('0P') == False
