class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """
        if x <2:
            return x
        left = 0
        right = x
        while abs(left-right) != 1:
            mid = (left+right) // 2
            sqrt = mid*mid
            if sqrt == x:
                return mid
            if sqrt > x:
                right = mid
            else:
                left = mid
        return left


if __name__ == "__main__":
    s = Solution()
    assert s.mySqrt(4) == 2
    assert s.mySqrt(8) == 2
