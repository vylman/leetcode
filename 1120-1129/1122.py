class Solution(object):
    def relativeSortArray(self, arr1, arr2):
        """
        :type arr1: List[int]
        :type arr2: List[int]
        :rtype: List[int]
        """
        hashmap = {}
        result1 = []

        for i in arr1:
            hashmap[i] = hashmap.get(i, 0) + 1

        for i in arr2:
            result1 += [i] * hashmap[i]
            hashmap.pop(i)

        result2 = []
        for key in hashmap:
            result2 += [key] * hashmap[key]
        result2.sort()
        return result1 + result2


if __name__ == '__main__':
    s = Solution()
    assert (s.relativeSortArray(arr1=[2, 3, 1, 3, 2, 4, 6, 7, 9, 2, 19], arr2=[2, 1, 4, 3, 9, 6])
            == [2, 2, 2, 1, 4, 3, 3, 9, 6, 7, 19])
    assert (s.relativeSortArray(arr1=[28, 6, 22, 8, 44, 17], arr2=[22, 28, 8, 6])
            == [22, 28, 8, 6, 17, 44])
