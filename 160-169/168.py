class Solution(object):
    def convertToTitle(self, columnNumber):
        """
        :type columnNumber: int
        :rtype: str
        """
        start_index = ord("A")


        res = ''

        while columnNumber > 0:
            columnNumber -= 1
            res += chr(start_index + columnNumber%26)
            columnNumber //= 26
        return res[::-1]


if __name__ == '__main__':
    s = Solution()
    assert s.convertToTitle(1) == 'A'
    assert s.convertToTitle(28) == 'AB'
    assert s.convertToTitle(701) == 'ZY'

