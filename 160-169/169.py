class Solution(object):
    def majorityElement(self, nums):

        count = 1
        result = nums[0]

        for num in nums[1:]:
            if num != result:
                count -= 1
                if count == 0:
                    result = num
                    count = 1
            else:
                count += 1

        return result