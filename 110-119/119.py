class Solution(object):
    def getRow(self, rowIndex):
        """
        :type rowIndex: int
        :rtype: List[int]
        """
        rowIndex = rowIndex+1
        res = []

        for i in range(1, rowIndex+1):
            res.append([1]*i)

        for i in range(rowIndex):
            for j in range(1, i):
                res[i][j] = res[i - 1][j - 1] + res[i - 1][j]
        return res[-1]

