class MyStack:

    def __init__(self):
        self.stack = []

    def push(self, x: int):
        self.stack.append(x)

    def pop(self) -> int:
        res = self.stack[-1]
        del self.stack[-1]
        return res

    def top(self) -> int:
        return self.stack[-1]

    def empty(self) -> bool:
        return len(self.stack) == 0