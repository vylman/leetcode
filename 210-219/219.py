class Solution(object):
    def containsNearbyDuplicate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        tmp_dict = {}
        for i in range(len(nums)):
            if nums[i] not in tmp_dict:
                tmp_dict[nums[i]] = i
            else:
                if abs(i - tmp_dict[nums[i]]) <= k:
                    return True
                else:
                    tmp_dict[nums[i]] = i

        return False


if __name__ == '__main__':
    s = Solution()
    #assert s.containsNearbyDuplicate([1, 2, 3, 1], 3) is True
    assert s.containsNearbyDuplicate(nums=[1, 0, 1, 1], k=1) is True
