class Solution(object):
    def containsDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        tmp = set()
        for i in nums:
            if i in tmp:
                return True
            else:
                tmp.add(i)
        return False



if __name__ == '__main__':
    s = Solution()
    assert s.containsDuplicate([1,2,3,1]) is True
    assert s.containsDuplicate([1,2,3,4]) is False