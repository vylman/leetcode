class Solution(object):
    def threeConsecutiveOdds(self, arr):
        """
        :type arr: List[int]
        :rtype: bool
        """
        count = 0
        for i in range(len(arr)):
            if not arr[i] % 2:
                count = 0
                continue
            else:
                count += 1
                if count == 3:
                    return True

        return False


if __name__ == "__main__":
    s = Solution()
    assert s.threeConsecutiveOdds([2, 6, 4, 1]) is False
    assert s.threeConsecutiveOdds([1, 2, 34, 3, 4, 5, 7, 23, 12]) is True
