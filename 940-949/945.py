class Solution(object):
    def minIncrementForUnique(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        count = 0
        nums.sort()

        for i in range(1, len(nums)):
            if nums[i] <= nums[i-1]:
                count += nums[i-1] - nums[i] + 1
                nums[i] = nums[i-1] + 1

        return count



if __name__ == "__main__":
    s = Solution()
    assert s.minIncrementForUnique([1,2,2]) == 1
    assert s.minIncrementForUnique([3,2,1,2,1,7]) == 6
